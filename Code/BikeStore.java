/**
 * Kelsey Costa
 * 2032587
 */
public class BikeStore {
    public static void main(String[]args){
        Bicycle[] Bikes = new Bicycle[4];

        Bikes[0] = new Bicycle("Nissan", 1, 1.5);
        Bikes[1] = new Bicycle("Ford", 2, 2.5);
        Bikes[2] = new Bicycle("Toyota", 3, 3.5);
        Bikes[3] = new Bicycle("Audi", 4, 4.5);

        for(Bicycle bike : Bikes){
            System.out.println(bike);
        }
    }
}
