/**
 * Kelsey Costa
 * 2032587
 */

 public class Bicycle{
     private String manufacturer;
     private int numberGears;
     private double maxSpeed;

     //constructor
    public Bicycle(String manu, int gears, double speed){
        this.manufacturer = manu;
        this.numberGears = gears;
        this.maxSpeed = speed;
    }
    //bunch of get methods
     public String getManufacturer(){
         return this.manufacturer;
     }
     public int getNumberGears(){
         return this.numberGears;
     }
     public double getMaxSpeed(){
         return this.maxSpeed;
     }

     public String toString(){
         return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", Max Speed: " + this.maxSpeed;
     }

 }